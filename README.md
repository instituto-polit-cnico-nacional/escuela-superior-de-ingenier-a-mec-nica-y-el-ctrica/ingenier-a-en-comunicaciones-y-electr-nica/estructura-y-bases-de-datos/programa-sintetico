# PROGRAMA SINTÉTICO

|                  |                                             |
| :---             | :---                                        |
| **CARRERA:**     | Ingeniería en Comunicaciones y Electrónica  |
| **ASIGNATURA:**  | Estructura y Bases de Datos                 |
| **SEMESTRE:**    | Tercero                                     |


## OBJETIVO GENERAL

El alumno diseñará programas para la resolución de problemas de ingeniería y ciencias, utilizando las estructuras de datos.

## CONTENIDO SINTÉTICO

1. Representación de bajo y alto nivel de datos.
1. Manejo de estructuras dinámicas implementadas con programación orientada a objetos.
1. Listas enlazadas.
1. Pilas y colas.
1. Archivos y recursividad.
1. Listas no lineales: árboles.
1. Listas no lineales: grafos.

## METODOLOGÍA

* Se utilizará la metodología inductiva para el aprendizaje o deductiva según sea el caso.
* Participación activa por parte de los alumnos con la guía del profesor.
* Búsqueda, lectura y análisis de información por parte de los alumnos.
* Solución de problemas en el aula y en el laboratorio de cómputo, con ayuda del profesor.

## EVALUACIÓN Y ACREDITACIÓN

* Tres exámenes departamentales
* Entrega de trabajos realizados en clase, tareas y prácticas de laboratorio.
* Participación en actividades individuales y de equipo.

## BIBLIOGRAFIA

* Robert Sedgewick - [Algoritmos en C++](activos/pdfs/R0B3RT_S3DG3W1CK_-_[4LG0R1TM0S_3N_C++]_-_4DD1S0N_W3SL3Y.pdf) - Addison Wesley.
* Cevallos F. J. - [Programación Orientada a Objetos con C++](activos/pdfs/C3V4LL0S_F._J._-_[PR0GR4M4C10N_0R13NT4D4_4_0BJ3T0S_C0N_C++]_-_R4-M4.pdf) - Ra-Ma.
* Joyanes Aguilar - [Programación en C++, algoritmos, estructuras de datos y objetos](activos/pdfs/J0Y4N3S_4GU1L4R_-_[PR0GR4M4C10N_3N_C++,_4LG0R1TM0S,_3STRUCTUR4S_D3_D4T0S_Y_0BJ3T0S]_-_MC_GR4W_H1LL.pdf) - Mc Graw Hill.

|                                                               |                                              |
| :---                                                          | :---                                         |
| **ESCUELA:** Superior de Ingeniería Mecánica y Eléctrica      | **ASIGNATURA:** Estructura y bases de datos  |
| **UNIDADES:** Culhuacan y Zacatenco                           | **SEMESTRE:** Tercero                        |
| **CARRERA:** Ingeniería en Comunicaciones y Electrónica       | **CLAVE:**                                   |
| **OPCIÓN:**                                                   | **CRÉDITOS:** 7.5                            |
| **COORDINACIÓN:** Academia de Computación                     | **VIGENTE:**                                 |
| **DEPARTAMENTO:** Ingeniería en Comunicaciones y Electrónica  | **TIPO DE ASIGNATURA:** Teórico - Práctica   |
|                                                               | ***MODALIDAD: Escolarizada***                |

| TIEMPOS ASIGNADOS          | HORAS |
| :---                       | :---  |
| **HRS/SEMANA/TEORÍA:**     | 3.0   |
| **HRS/SEMANA/PRÁCTICA:**   | 1.5   |
| **HRS/SEMESTRE/TEORÍA:**   | 54.0  |
| **HRS/SEMESTRE/PRÁCTICA:** | 27.0  |
| **HRS/TOTALES:**           | 81.0  |

|                                                                                          |                                                                                                       |
| :---                                                                                     | :---                                                                                                  |
| **PROGRAMA ELABORADO O ACTUALIZADO POR:** Academia de Computación Culhuacan y Zacatenco  | **AUTORIZADO POR:** Comisión de Planes y Programas de Estudio del Consejo General Consultivo del IPN  |
| **REVISADO POR:** Subdirecciones Académicas de ESIME Culhuacan y Zacatenco               |                                                                                                       |
| **APROBADO POR:** Los Consejos Técnicos Consultivos Escolares de la ESIME Culhuacan Ing. Fermín Valencia Figueroa y ESIME Zacatenco Dr. Alberto Cornejo Lizarralde |                             |
| **DEPARTAMENTO:** Ingeniería en Comunicaciones y Electrónica                             |                                                                                                       |

## FUNDAMENTACIÓN DE LA ASIGNATURA

En el campo de la ingeniería moderna es requerido el uso de la computadora como una herramienta principal en el diseño y simulación de sistemas, para ello el alumno requiere del conocimiento de las estructuras y bases de datos, que le permiten manejar, representar y administrar grandes volúmenes de datos para resolver problemas de ingeniería con eficacia empleando una computadora.

Los antecedentes necesarios para cursar esta materia son los cursos de Fundamentos de Programación y Programación Orientada a Objetos que se imparten en el primer y segundo semestre de las carreras de ingeniería.

## OBJETIVO DE LA ASIGNATURA

El alumno diseñará programas para la resolución de problemas de ingeniería y ciencias, utilizando las estructuras de
datos.

## RELACION DE PRÁCTICAS

| No. PRÁCTICA  | NOMBRE DE LA PRÁCTICA                                     | UNIDAD  | DURACIÓN  | LUGAR DE REALIZACIÓN  |
| :---          | :---                                                      | :----:  | :----:    | :---                  |
| 1             | Capacidad de los diferentes tipos de datos                | I       | 3.0       | Lab. Computación      |
| 2             | Tipos de Estructuras de Datos                             | II      | 1.5       | :heavy_check_mark:    |
| 3             | Tipos de Estructuras de Datos implementados con POO       | II      | 1.5       | :heavy_check_mark:    |
| 4             | Ordenamiento de Listas simplemente enlazadas              | III     | 1.5       | :heavy_check_mark:    |
| 5             | Ordenamiento y recorrido de Listas doblemente enlazadas   | III     | 1.5       | :heavy_check_mark:    |
| 6             | Pilas implementadas con POO                               | IV      | 1.5       | :heavy_check_mark:    |
| 7             | Colas implementadas con POO                               | IV      | 1.5       | :heavy_check_mark:    |
| 8             | Operaciones de salvar y cargar en una Base de Datos       | V       | 3.0       | :heavy_check_mark:    |
| 9             | Recursividad para búsqueda binaria y ordenamiento rápido  | V       | 1.5       | :heavy_check_mark:    |
| 10            | Árboles no binarios                                       | VI      | 1.5       | :heavy_check_mark:    |
| 11            | Operaciones de agregar y eliminar en árboles binarios     | VI      | 3.0       | :heavy_check_mark:    |
| 12            | Recorrido en anchura y profundidad en un árbol binario    | VI      | 1.5       | :heavy_check_mark:    |
| 13            | Grafos                                                    | VII     | 4.5       | :heavy_check_mark:    |


| PERIODO       | UNIDAD      | PROCEDIMIENTO DE EVALUACIÓN                |
| :----:        | :----:      | :---                                       |
| 1             | I y II      | 50% Examen<br>30% Prácticas<br>20% Tareas  |
| 2             | III y IV    | 50% Examen<br>30% Prácticas<br>20% Tareas  |
| 3             | V y VI      | 50% Examen<br>30% Prácticas<br>20% Tareas  |
|               |             | El alumno tendrá derecho a los exámenes de periodo y extraordinario si cumple con el 100% de las prácticas, tareas e investigaciones  |

| CLAVE   | B       |C       |                                                                                                                                                         |
| :----:  | :----:  |:----:  | :---                                                                                                                                                    |
| 1       | :x:     |        | [Robert Sedgewick Algoritmos en C++ Addison Wesley](activos/pdfs/R0B3RT_S3DG3W1CK_-_[4LG0R1TM0S_3N_C++]_-_4DD1S0N_W3SL3Y.pdf)                           |
| 2       | :x:     |        | [Joyanes Aguilar Programación en C++, algoritmos, estructuras de datos y objetos, Mc Graw Hill](activos/pdfs/J0Y4N3S_4GU1L4R_-_[PR0GR4M4C10N_3N_C++,_4LG0R1TM0S,_3STRUCTUR4S_D3_D4T0S_Y_0BJ3T0S]_-_MC_GR4W_H1LL.pdf)  |
| 3       | :x:     |        | [Cevallos F. J. Programación Orientada a Objetos con C++, Ra-Ma](activos/pdfs/C3V4LL0S_F._J._-_[PR0GR4M4C10N_0R13NT4D4_4_0BJ3T0S_C0N_C++]_-_R4-M4.pdf)  |
| 4       |         | :x:    | Aho, Hopcroft; Ullman, Estructura de datos y algoritmos, Addison Wesley                                                                                 |
| 5       |         | :x:    | Kruse Robert L., Estructura de datos y diseño de programas, Prentice-Hall                                                                               |
| 6       |         | :x:    | Ford, Topp, Data Structures with C++, using STL, Prentice Hall                                                                                          |
| 7       |         | :x:    | Devis Botella Ricardo C++ STL Plantillas, Excepciones , Roles y Objetos, Paraninfo                                                                      |
| 8       |         | :x:    | Schildt Herbert C++ Guía de autoenseñanza, McGraw Hill                                                                                                  |
| 9       |         | :x:    | [Heileman Gregory L. Estructura de datos, algoritmos y Programación Orientada a Objetos, Mc Graw Hill](http://digital.cftsa.cl/elibros/ESTRUCTURAS%20DE%20DATOS%20ALGORITMOS%20Y%20PROGRAMACION%20ORIENTADA%20A%20OBJETOS/)  |
| 10      |         | :x:    | Musser, Saini, STL tutorial and reference guide, Addison Wesley.                                                                                        |
| 11      |         | :x:    | Johnsonbaugh Richard, Matemáticas Discretas                                                                                                             |
| 12      |         | :x:    | Kolman Bernard, Busby Robert, Estructuras de Matemáticas. discretas para la computación, Prentice-Hall                                                  |
|         |         |        | **libros en línea**                                                                                                                                     |
| 13      |         | :x:    | http://www.tcfb.com/freetechbooks/bookcpp.html                                                                                                          |
| 14      |         | :x:    | http://www.codeproject.com/books/                                                                                                                       |
| 15      |         | :x:    | http://ebrahimi.20fr.com/prbooks.htm                                                                                                                    |
|         |         |        | **cursos**                                                                                                                                              |
| 16      |         | :x:    | http://csa.iisc.ernet.in/resources/documentation/tutorials/C++/index.html                                                                               |
| 17      |         | :x:    | http://www.gotdotnet.com/team/cplusplus/                                                                                                                |
| 18      |         | :x:    | http://www.learnvisualstudio.net/                                                                                                                       |

## PERFIL DOCENTE POR ASIGNATURA

1. DATOS GENERALES

   |                                                                                           |
   | :---                                                                                      |
   | **ESCUELA:** Superior de Ingeniería Mecánica y Eléctrica                                  |
   | **CARRERA:** Ingeniería en Comunicaciones y Electrónica                                   |
   | **SEMESTRE:** Tercero                                                                     |
   | **ÁREA:** C. INGENIERÍA                                                                   |
   | **ACADEMIA:**                                                                             |
   | **ASIGNATURA:** Estructura y bases de datos                                               |
   | **ESPECIALIDAD Y NIVEL ACADÉMICO REQUERIDO:** Licenciatura En Ingeniería y/o Matemáticas  |

2. OBJETIVOS DE LA ASIGNATURA: El alumno diseñará programas para la resolución de problemas de ingeniería y ciencias, utilizando las estructuras de datos.

3. PERFIL DOCENTE:

   | CONOCIMIENTOS                                    | EXPERIENCIA PROFESIONAL                                    | HABILIDADES                                     | ACTITUDES  |
   | :---                                             | :---                                                       | :---                                            | :---       |
   | En el área de las matemáticas y la programación  | Mínimo un año  impartiendo clases en  alguna materia afín  | En el manejo de grupos.<br><br> Facilitador del conocimiento.<br><br> Dominio de la asignatura.<br><br> Manejo de material didáctico.  | <ul><li>Tolerante</li><li>Respetuosa</li><li>Empática</li><li>Responsabilidad</li><li>Científica</li><li>Superación</li><li>Compromiso Social</li></ul>  |

   | ELABORÓ                                            | REVISÓ                                             | AUTORIZÓ                                           |
   | :----:                                             | :----:                                             | :----:                                             |
   | <br><br><br>_____________________________________  | <br><br><br>_____________________________________  | <br><br><br>_____________________________________  |
   | PRESIDENTE DE ACADEMIA<br>Ing. Aurelio Gómez Velázquez.<br>Computación Culhuacan.<br>Ing. José Luis Bravo León.<br>Computación Zacatenco.  | SUBDIRECTOR ACADÉMICO<br>M. en C. Alberto Paz Gutiérrez.<br>ESIME Culhuacan<br>Ing. Guillermo Santillán Guevara.<br>ESIME Zacatenco  | DIRECTOR DEL PLANTEL<br>Ing. Fermín Valencia Figueroa.<br>ESIME Culhuacan<br>Dr. Alberto Cornejo Lizarralde.<br>ESIME Zacatenco  |
   
   **FECHA:** Marzo de 2004




